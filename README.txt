This directory contains two sample Python scripts demonstrating use of the Discrete Fourier Transform from the numpy.fft module and the Butterworth filter from the scipy.signal library.

The other two files contain a data set from the Actitracker data. 

The .accdata file contains data in the format

timestamp, x_accel, y_accel, z_accel, x_gyro, y_gyro, z_gyro;

The .csv omits the final ';'
