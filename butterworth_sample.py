from numpy import genfromtxt
import numpy as np
from matplotlib import pyplot as plt
import scipy.signal as signal

f_name = '/Users/luigi/Desktop/Gait/signal_processing/sample.csv'

sensor_data = genfromtxt(f_name, delimiter=',')

start=100
end = 356

s_ts_l = [sensor_data[i][0] for i in range(start, end)]
s_x_acc_l = [sensor_data[i][1] for i in range(start, end)]
s_y_acc_l = [sensor_data[i][2] for i in range(start, end)]
s_z_acc_l = [sensor_data[i][3] for i in range(start, end)]

s_x_gyro_l = [sensor_data[i][4] for i in range(start, end)]
s_y_gyro_l = [sensor_data[i][5] for i in range(start, end)]
s_z_gyro_l = [sensor_data[i][6] for i in range(start, end)]


# First, design the Buterworth filter
N  = 2    # Filter order
Wn = .3   # Cutoff frequency
B, A = signal.butter(N, Wn, output='ba')

# Second, apply the filter
xab = signal.filtfilt(B,A, s_x_acc_l)
yab = signal.filtfilt(B,A, s_y_acc_l)
zab = signal.filtfilt(B,A, s_z_acc_l)

xgb = signal.filtfilt(B,A, s_x_gyro_l)
ygb = signal.filtfilt(B,A, s_y_gyro_l)
zgb = signal.filtfilt(B,A, s_z_gyro_l)

r=2
c=3
i=1

plt.subplot(r,c,i)
plt.title("x accel")  
plt.plot(s_ts_l,s_x_acc_l, color='red', label='Signal')  
plt.plot(s_ts_l, xab, color='blue', label='Filter')
plt.legend()

i=i+1

plt.subplot(r,c,i)
plt.title("y accel")  
plt.plot(s_ts_l,s_y_acc_l, color='red', label='Signal')  
plt.plot(s_ts_l, yab, color='blue', label='Filter')

i=i+1

plt.subplot(r,c,i)
plt.title("z accel")  
plt.plot(s_ts_l,s_z_acc_l, color='red', label='Signal')  
plt.plot(s_ts_l, zab, color='blue', label='Filter')

i=i+1

plt.subplot(r,c,i)
plt.title("x gyro")  
plt.plot(s_ts_l,s_x_gyro_l, color='red', label='Signal')  
plt.plot(s_ts_l, xgb, color='blue', label='Filter')
plt.legend()

i=i+1

plt.subplot(r,c,i)
plt.title("y gyro")  
plt.plot(s_ts_l,s_y_gyro_l, color='red', label='Signal')  
plt.plot(s_ts_l, ygb, color='blue', label='Filter')

i=i+1

plt.subplot(r,c,i)
plt.title("z gyro")  
plt.plot(s_ts_l,s_z_gyro_l, color='red', label='Signal')  
plt.plot(s_ts_l, zgb, color='blue', label='Filter')


plt.show()

"""
plt.subplot(2,3,4)
plt.plot(s_ts_l, s_x_gyro_l, color='blue', label='x gyro')
plt.legend()
plt.subplot(2,3,5)
plt.plot(s_ts_l, s_y_gyro_l, color='blue', label='y gyro')
plt.legend()
plt.subplot(2,3,6)
plt.plot(s_ts_l, s_z_gyro_l, color='blue', label='z gyro')
plt.legend()
plt.show()
"""